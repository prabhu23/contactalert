package com.contactcallnotification.persentation.viewmodel

import android.annotation.SuppressLint
import android.app.Application
import android.provider.ContactsContract
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.contactcallnotification.domain.model.Callalert
import com.contactcallnotification.domain.usecase.AddCallAlertUseCase
import com.contactcallnotification.domain.usecase.CallAlertUseCases
import com.contactcallnotification.util.ContactListState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ContactViewModel @Inject constructor(
    private val application: Application,
    private val callAlertUseCases: CallAlertUseCases
): ViewModel() {

//    val contactLiveData: MutableLiveData<List<Callalert>> by lazy {
//        MutableLiveData<List<Callalert>>()
//    }

    val contactStatLiveData: MutableLiveData<ContactListState> by lazy {
        MutableLiveData<ContactListState>()
    }

    fun getContactList(){
        viewModelScope.launch(Dispatchers.IO){
            contactStatLiveData.postValue(ContactListState(true, getContacts()))
        }
    }
    fun insertCallAlert(callalert: Callalert?) {
        viewModelScope.launch(Dispatchers.IO) {
            callAlertUseCases.addCallAlertUseCase(callalert!!)
        }
    }
    fun deleteCallAlert(callalert: Callalert?) {
        var contactLiveDataUpdate : List<Callalert>? = contactStatLiveData.value?.contactListState
        contactLiveDataUpdate?.map { it-> if(it.number == callalert?.number) it.isSelected = false }
        viewModelScope.launch(Dispatchers.IO) {
            callAlertUseCases.deleteCallAlertNubmers(callalert!!)
        }
    }


    @SuppressLint("Range")
    private fun getContacts(): ArrayList<Callalert> {
        var getSaveCallAlertList: List<Callalert> = callAlertUseCases.getCallAlertNubmers();
        val contacts = ArrayList<Callalert>()
        val cursor = application.contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC")
        cursor?.let {

            while (cursor.moveToNext()) {
                val id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID))
                val name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
                val phoneNumber = (cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))).toInt()

                if (phoneNumber > 0) {
                    val cursorPhone = application.contentResolver.query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?", arrayOf(id), null)

                    if (cursorPhone!!.count > 0) {
                        while (cursorPhone.moveToNext()) {
                            val phoneNumValue = cursorPhone.getString(cursorPhone.getColumnIndex(
                                ContactsContract.CommonDataKinds.Phone.NUMBER))

                            var isSelectedContact = false
                            var id = 0
                            getSaveCallAlertList.map { it-> if(it.number == phoneNumValue){
                                isSelectedContact = true
                                id = it.id
                            }
                            }
                            contacts.add(Callalert(name, phoneNumValue,isSelectedContact,id))

                        }
                    }
                    cursorPhone.close()
                }
            }
        }
        cursor?.close()


        return contacts
    }
}