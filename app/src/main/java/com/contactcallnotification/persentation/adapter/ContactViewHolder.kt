package com.contactcallnotification.persentation.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.contactcallnotification.R
import com.contactcallnotification.domain.model.Callalert

class ContactViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var contactNameItemView: TextView? = null
    var contactnumberItemView: TextView? = null
    var saveContact: CheckBox? = null

    init {
        contactNameItemView = itemView.findViewById(R.id.tvName)
        contactnumberItemView = itemView.findViewById(R.id.tvNumber)
        saveContact = itemView.findViewById(R.id.selectedAlert)
    }

    fun bind(callalert: Callalert) {
        contactNameItemView?.setText(callalert.name)
        contactnumberItemView?.setText(callalert.number)
        if(callalert.isSelected){
            saveContact?.setChecked(true)
        }else{

            saveContact?.setChecked(false)
        }

    }
}