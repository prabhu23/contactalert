package com.contactcallnotification.persentation.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.contactcallnotification.R
import com.contactcallnotification.domain.model.Callalert
import com.contactcallnotification.domain.usecase.CallAlertUseCases
import com.contactcallnotification.persentation.viewmodel.ContactViewModel
import javax.inject.Inject

class ContactAdapter  ( private val contactViewModel: ContactViewModel) : ListAdapter<Callalert, ContactViewHolder>(DiffCallback())  {
    private   val TAG = "ContactAdapter"
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
        return ContactViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        holder.bind(getItem(position))
        holder.saveContact?.setOnClickListener(View.OnClickListener {
            if (holder.saveContact!!.isChecked) {
                contactViewModel.insertCallAlert(getItem(position))
            }else {
//                    buttonView.setChecked(false)
                contactViewModel.deleteCallAlert(getItem(position))
            }
        })
//        holder.saveContact?.setOnCheckedChangeListener { buttonView, isChecked ->
//
//                if (isChecked) {
//                    contactViewModel.insertCallAlert(getItem(position))
//                }else {
////                    buttonView.setChecked(false)
//                    contactViewModel.deleteCallAlert(getItem(position))
//                }
//
//
//        }
    }


}

class DiffCallback : DiffUtil.ItemCallback<Callalert>() {
    override fun areItemsTheSame(oldItem: Callalert, newItem: Callalert): Boolean {
        return oldItem?.id == newItem?.id
    }

    override fun areContentsTheSame(oldItem: Callalert, newItem: Callalert): Boolean {
        return oldItem == newItem
    }
}