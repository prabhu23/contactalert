package com.contactcallnotification.persentation.screen

import android.Manifest
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.contactcallnotification.databinding.ActivityMainBinding
import com.contactcallnotification.domain.model.Callalert
import com.contactcallnotification.persentation.adapter.ContactAdapter
import com.contactcallnotification.persentation.viewmodel.ContactViewModel
import com.contactcallnotification.util.Constans
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    var overlayEnabled = false

    private val contactViewModel: ContactViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if ((ContextCompat.checkSelfPermission(this@MainActivity,
                Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
                this@MainActivity,
                Manifest.permission.SYSTEM_ALERT_WINDOW) != PackageManager.PERMISSION_GRANTED) &&
            (ContextCompat.checkSelfPermission(this@MainActivity,
                Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED )
        ) {

        }
    }


    override fun onResume() {
        super.onResume()
        if (!Settings.canDrawOverlays(this)) {
            openOverlaySettings()
        }else{
            loadContacts()
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private fun openOverlaySettings() {
        val intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
            Uri.parse("package:$packageName"))
        try {
            startActivityForResult(intent, Constans.PERMISSIONS_OVERLAY_SETTING)
        } catch (e: ActivityNotFoundException) {
            Log.e("error", e.printStackTrace().toString())
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            Constans.PERMISSIONS_OVERLAY_SETTING -> {
                overlayEnabled = Settings.canDrawOverlays(this)

                loadContacts()
            }

        }
    }
    private fun loadContacts() {
        if ((ContextCompat.checkSelfPermission(this@MainActivity,
                Manifest.permission.READ_CALL_LOG) == PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
                this@MainActivity,
                Manifest.permission.SYSTEM_ALERT_WINDOW) == PackageManager.PERMISSION_GRANTED) &&
            (ContextCompat.checkSelfPermission(this@MainActivity,
                Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED  )
        ) {
            binding.recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
            val adapter = ContactAdapter(contactViewModel)

            binding.recyclerView.adapter = adapter
        contactViewModel.contactStatLiveData.observe(this, Observer {
            if(it.isNotesOrderShow){
                binding.progressBar.hide()
            }
            adapter.submitList(it.contactListState)
            adapter.notifyDataSetChanged()
        })
            contactViewModel.getContactList()
        }else {
            ActivityCompat.requestPermissions(this@MainActivity,
                arrayOf(Manifest.permission.READ_CALL_LOG,
                    Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.READ_CONTACTS,
                    Manifest.permission.SYSTEM_ALERT_WINDOW),
                Constans.PERMISSIONS_REQUEST_READ_CONTACTS)
            overlayEnabled = true
        }


    }

    @SuppressLint("MissingSuperCall")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == Constans.PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                loadContacts()
            } else {
                showToast("Permission must be granted in order to display contacts information")
            }
        }
    }

    private fun showToast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }
}

