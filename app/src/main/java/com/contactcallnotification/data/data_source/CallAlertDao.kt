package com.contactcallnotification.data.data_source

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.contactcallnotification.domain.model.Callalert

@Dao
interface CallAlertDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(callAlert: Callalert)

    @Query("DELETE FROM callalert WHERE id = :id")
    fun deleteById(id: Int): Int

    @Query("SELECT * FROM callalert ")
    fun getCallAlertList(): List<Callalert>

}