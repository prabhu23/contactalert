package com.contactcallnotification.data.data_source

import androidx.room.Database
import androidx.room.RoomDatabase
import com.contactcallnotification.domain.model.Callalert

@Database(
    entities =[Callalert::class],
    version = 1
)
abstract class CallAlertDatabase : RoomDatabase() {

    abstract val callAlertDao: CallAlertDao;

    companion object{
        val DATABASE_NAME = "callalert_db"
    }
}