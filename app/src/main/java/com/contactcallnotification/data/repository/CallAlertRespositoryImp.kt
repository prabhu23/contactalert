package com.contactcallnotification.data.repository

import com.contactcallnotification.data.data_source.CallAlertDao
import com.contactcallnotification.domain.model.Callalert
import com.contactcallnotification.domain.repository.CallAlertRespository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class CallAlertRespositoryImp  (private val callAlertDao: CallAlertDao): CallAlertRespository {
    override fun getCallAlert(): List<Callalert> {
        return callAlertDao.getCallAlertList()
    }

//    override suspend fun getCallAlertById(id: Int): Callalert? {
//        callAlertDao.get
//    }

    override suspend fun insetCallAlert(callAlert: Callalert) {
         callAlertDao.insert(callAlert)
    }

    override suspend fun deletCallAlert(callAlert: Callalert) {
        callAlert.id?.let { callAlertDao.deleteById(it) }
    }

}