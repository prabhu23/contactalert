package com.contactcallnotification.util

import com.contactcallnotification.domain.model.Callalert

data class ContactListState(  var isNotesOrderShow: Boolean = false, var contactListState :List<Callalert> = emptyList())