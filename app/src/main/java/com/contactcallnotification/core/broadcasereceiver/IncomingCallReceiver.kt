package com.contactcallnotification.core.broadcasereceiver

import android.content.Context
import android.os.Build
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.contactcallnotification.R
import com.contactcallnotification.databinding.MessageBoxBinding
import com.contactcallnotification.domain.model.Callalert
import com.contactcallnotification.domain.usecase.CallAlertUseCases
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
open class IncomingCallReceiver: PhoneCallReceiver() {

    @Inject lateinit var  callAlertUseCases:CallAlertUseCases
    override fun onIncomingCallStarted (context: Context?, number: String?, start: Date?) {
        Log.e("CallReceiverNumber", number!!)
        Toast.makeText(context,
            "onIncomingCallStarted  $number",
            Toast.LENGTH_SHORT).show()
        var isNumberAdded = false
        CoroutineScope(Dispatchers.IO).launch {
            ///
            isNumberAdded = callAlertUseCases.isCallAlertNubmers(number)
        withContext(Dispatchers.Main) {

        if (number.isNotEmpty() && number.isNotBlank() && isNumberAdded ) {
            val sdf = SimpleDateFormat("hh:mm:ss aa")
            val currentDate = sdf.format(Date())

            val bind: MessageBoxBinding = MessageBoxBinding.inflate(LayoutInflater.from(context))

            val dialogBuilder =
                AlertDialog.Builder(context!!, R.style.Theme_Truecaller).setView(bind.root)
            bind.textViewNumber.text = number
            bind.textViewTime.text = context.resources.getText(R.string.incoming_call).toString() + "\n" + currentDate
            dialogBuilder.setCancelable(false)
            val dialog = dialogBuilder.create()
            val dialogWindow: Window? = dialog.window
            val dialogWindowAttributes: WindowManager.LayoutParams = dialogWindow!!.attributes

            val lp = WindowManager.LayoutParams()
            lp.copyFrom(dialogWindowAttributes)
            lp.width = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                280f,
                context.resources.displayMetrics)
                .toInt()
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT
            dialogWindow.attributes = lp

            // Set to TYPE_SYSTEM_ALERT so that the Service can display it
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                dialogWindow.setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY)
            } else {
                dialogWindow.setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT)
            }
//        dialogWindowAttributes.windowAnimations = R.style.DialogAnimation
            dialog.show()

            bind.textViewClose.setOnClickListener {
                dialog.dismiss()
            }
        }
        }
        }
    }

    override fun onOutgoingCallStarted(ctx: Context?, number: String?, start: Date?) {
        Toast.makeText(ctx, "onOutgoingCallStarted", Toast.LENGTH_SHORT).show()
    }

    override fun onIncomingCallEnded(ctx: Context?, number: String?, start: Date?, end: Date?) {
        Toast.makeText(ctx, "onIncomingCallEnded", Toast.LENGTH_SHORT).show()
    }

    override fun onOutgoingCallEnded(ctx: Context?, number: String?, start: Date?, end: Date?) {
        Toast.makeText(ctx, "onOutgoingCallEnded", Toast.LENGTH_SHORT).show()
    }

    override fun onMissedCall(ctx: Context?, number: String?, start: Date?) {
        Toast.makeText(ctx, "onMissedCall", Toast.LENGTH_SHORT).show()
    }
}