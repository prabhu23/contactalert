package com.contactcallnotification.domain.model

import android.net.Uri
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Callalert(
    var name: String,
    var number: String,
    var isSelected: Boolean =false,
    @PrimaryKey(autoGenerate = true) val id: Int =0
)

class InvalidNoteException(message: String): Exception(message)