package com.contactcallnotification.domain.usecase

import android.util.Log
import com.contactcallnotification.domain.model.Callalert
import com.contactcallnotification.domain.model.InvalidNoteException
import com.contactcallnotification.domain.repository.CallAlertRespository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.count
import kotlinx.coroutines.flow.filter
import kotlin.jvm.Throws

class IsCallAlertNubmers (private val repository: CallAlertRespository) {
    private   val TAG = "GetCallAlertNubmers"
    @Throws(InvalidNoteException::class)
      operator fun invoke(callNubmer: String): Boolean {

       var contactList =  repository.getCallAlert().filter { callalert -> callNubmer==callalert.number }
        Log.i(TAG, "invoke: ${contactList.count()}")
         if (contactList.count() > 0) {return true } else{ return false}

    }
}