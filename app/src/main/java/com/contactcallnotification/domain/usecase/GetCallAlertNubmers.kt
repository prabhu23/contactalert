package com.contactcallnotification.domain.usecase

import android.util.Log
import com.contactcallnotification.domain.model.Callalert
import com.contactcallnotification.domain.model.InvalidNoteException
import com.contactcallnotification.domain.repository.CallAlertRespository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.count
import kotlinx.coroutines.flow.filter
import kotlin.jvm.Throws

class GetCallAlertNubmers (private val repository: CallAlertRespository) {
    private   val TAG = "GetCallAlertNubmers"

      operator fun invoke( ): List<Callalert> {
      return repository.getCallAlert()
    }
}