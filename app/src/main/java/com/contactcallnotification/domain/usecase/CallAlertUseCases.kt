package com.contactcallnotification.domain.usecase

data class CallAlertUseCases ( val addCallAlertUseCase: AddCallAlertUseCase,
                               val getCallAlertNubmers: GetCallAlertNubmers,
                               val isCallAlertNubmers: IsCallAlertNubmers,
                               val deleteCallAlertNubmers: DeleteCallAlertNubmers)