package com.contactcallnotification.domain.usecase

import android.util.Log
import com.contactcallnotification.domain.model.Callalert
import com.contactcallnotification.domain.model.InvalidNoteException
import com.contactcallnotification.domain.repository.CallAlertRespository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.count
import kotlinx.coroutines.flow.filter
import kotlin.jvm.Throws

class DeleteCallAlertNubmers (private val repository: CallAlertRespository) {
    private   val TAG = "GetCallAlertNubmers"

      suspend operator fun invoke(callalert: Callalert )  {
       return repository.deletCallAlert(callalert)
    }
}