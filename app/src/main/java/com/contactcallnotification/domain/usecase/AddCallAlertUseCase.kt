package com.contactcallnotification.domain.usecase

import com.contactcallnotification.domain.model.Callalert
import com.contactcallnotification.domain.model.InvalidNoteException
import com.contactcallnotification.domain.repository.CallAlertRespository
import kotlin.jvm.Throws

class AddCallAlertUseCase (private val repository: CallAlertRespository) {

    @Throws(InvalidNoteException::class)
    suspend operator fun invoke(callalert: Callalert){
        if(callalert.name.isBlank()){
            throw InvalidNoteException("The title of the note can't be empty.")
        }
        if(callalert.number.isBlank()){
            throw InvalidNoteException("The content of the note can't be empty.")
        }
         repository.insetCallAlert(callalert = callalert)
    }
}