package com.contactcallnotification.domain.repository

import com.contactcallnotification.domain.model.Callalert
import kotlinx.coroutines.flow.Flow

interface CallAlertRespository {
    fun getCallAlert():  List<Callalert>

//    suspend fun getCallAlertById(id: Int): Callalert?

    suspend fun insetCallAlert(callalert: Callalert)

    suspend fun deletCallAlert(callalert: Callalert)
}