package com.contactcallnotification.di

import android.app.Application
import androidx.room.Room
import androidx.room.RoomDatabase
import com.contactcallnotification.data.data_source.CallAlertDatabase
import com.contactcallnotification.data.repository.CallAlertRespositoryImp
import com.contactcallnotification.domain.repository.CallAlertRespository
import com.contactcallnotification.domain.usecase.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun proviedCallAlertDB(app: Application): CallAlertDatabase{
        return Room.databaseBuilder(
            app,
            CallAlertDatabase::class.java,
            CallAlertDatabase.DATABASE_NAME
        ).build()
    }

    @Provides
    @Singleton
    fun proviedCallAlertRepository(db: CallAlertDatabase): CallAlertRespository {
        return CallAlertRespositoryImp(db.callAlertDao)
    }

    @Provides
    @Singleton
    fun proviedCallAlertUserCases(repository: CallAlertRespository):CallAlertUseCases{
        return CallAlertUseCases( AddCallAlertUseCase(repository = repository),
            GetCallAlertNubmers(repository),
            IsCallAlertNubmers(repository),
            DeleteCallAlertNubmers(repository)
        )
    }

}